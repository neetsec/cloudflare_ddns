#!/bin/sh
# Log file to store the last public IP to compare with
lastIPLog=/var/opt/cloudflare_ddns/.lastIPLog

# Check if the file exist or create it if not
if [ ! -e "$lastIPLog" ] ; then
    touch "$lastIPLog"
fi

if [ ! -w "$lastIPLog" ] ; then
    echo cannot write to $lastIPLog
    exit 1
fi

# Import data from cloudflare
set -o allexport
. /var/opt/cloudflare_ddns/.env
set +o allexport

#Get the IPV4 local Address.
PUBLIC_IPV4=`curl -s https://ifconfig.me`
#Get the lastIP logged
lastIP=$(cat "$lastIPLog")
#Get IP from CloudFlare by the API V4
resolvedDNSIP=$(./zonesData.sh | jq -r '.content')

#if the last IP is diferent from the current IP (some change in the ISP)
#or the resolved address is diferent from current IP (some change in the DNS)
if [ $lastIP != $PUBLIC_IPV4 ] || [ $resolvedDNSIP != $PUBLIC_IPV4 ]; then
  #Update DNS by the CloudFlare API v4 with the values from the .env file
  curl -X PUT "https://api.cloudflare.com/client/v4/zones/"$ZONE_ID"/dns_records/"$ID \
       -H	"Authorization: Bearer "$API_KEY\
       -H "Content-Type:application/json"\
       --data '{"type":"'$DNS_TYPE'","name":"'$DOMAIN_NAME'","content":"'$PUBLIC_IPV4'","ttl":120,"proxied":false}' >> /tmp/DDNS.log  2>&1 &
  echo "$PUBLIC_IPV4" > "$lastIPLog"
else
  #log the same IP and date where it was tested if is still the same
  echo "At "`date -u`" the IP is still $lastIP" >> /tmp/DDNS.log
  echo "$PUBLIC_IPV4" > "$lastIPLog"
fi

#KEEP IT SIMPLE AND STUPID!
